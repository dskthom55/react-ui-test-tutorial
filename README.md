This is an example project to act as a simple playground for comparing Enzyme and React Testing Library tests. It was created using [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.  
Open [http://localhost:3030](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn test-coverage`

Runs unit tests with a coverage report.

### `yarn validate`

Runs `prettier`, the TypeScript compiler, and `test-coverage`

## Credits
* All Pokemon data comes from [PokeAPI](https://pokeapi.co/).  
* Pokeball icon: https://www.deviantart.com/beccerberry/art/Pokeball-Desktop-icon-200305023
