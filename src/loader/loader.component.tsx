import React from "react";
import injectSheet from "react-jss";

const loadIcon = require("../assets/PokemonIcon~Beccerberry.ico");

type Props = {
  classes: any;
};

function LoaderComponent({ classes }: Props): React.ReactElement<any> {
  return (
    <img
      src={loadIcon}
      alt="Loading Icon Pokeball"
      className={classes.loader}
    />
  );
}

const styles = {
  loader: {
    animation: "$spin infinite 3s linear",
    width: 50,
    height: 50
  },
  "@keyframes spin": {
    from: {
      transform: "rotate(0deg)"
    },
    to: {
      transform: "rotate(360deg)"
    }
  }
};

export const Loader = injectSheet(styles)(LoaderComponent);
