import React from "react";
import injectSheet from "react-jss";
import { Stat, StatContainer } from "../make-request/poke-api.types";

type StatBlockProps = {
  stats: StatContainer[];
};

type Props = StatBlockProps & {
  classes: any;
};

function StatBlockComponent({
  classes,
  stats
}: Props): React.ReactElement<Props> {
  const formatStatName = (stat: Stat) =>
    stat
      .replace("-", " ")
      .replace("special", "sp")
      .toUpperCase();
  const getStat = (name: Stat): { name: string; value: number } => {
    const foundStat = stats.find(stat => stat.stat.name === name);
    return {
      name: formatStatName(name),
      value: foundStat ? foundStat.base_stat : 0
    };
  };
  const getStylesForStat = (stat: number) => {
    let backgroundColor;
    let fontColor;
    switch (true) {
      case stat <= 30:
        backgroundColor = "red";
        fontColor = "black";
        break;
      case stat <= 70:
        backgroundColor = "yellow";
        fontColor = "black";
        break;
      default:
        backgroundColor = "green";
        fontColor = "white";
        break;
    }
    return {
      backgroundColor,
      color: fontColor,
      width: stat * 2
    };
  };

  const statValues: { name: string; value: number }[] = [
    getStat(Stat.HP),
    getStat(Stat.Attack),
    getStat(Stat.Defense),
    getStat(Stat.SpecialAttack),
    getStat(Stat.SpecialDefense),
    getStat(Stat.Speed)
  ];

  return (
    <section>
      {statValues.map(statValue => (
        <div
          title={`${statValue.name}: ${statValue.value}`}
          className={classes.statBar}
          style={getStylesForStat(statValue.value)}
          key={statValue.name}
        >
          {statValue.name}: {statValue.value}
        </div>
      ))}
    </section>
  );
}

const styles = {
  statBar: {
    height: 20,
    margin: {
      bottom: 5,
      left: 3
    },
    padding: {
      left: 5
    },
    fontSize: 15,
    display: "flex"
  }
};

export const StatBlock = injectSheet(styles)(StatBlockComponent);
