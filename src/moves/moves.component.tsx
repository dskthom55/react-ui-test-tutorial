import React, { useState } from "react";
import injectSheet from "react-jss";
import { Move } from "../make-request/poke-api.types";

type MovesProps = {
  classes: any;
  moves: Move[];
};

export function MovesComponent({
  classes,
  moves
}: MovesProps): React.ReactElement<MovesProps> {
  const [isShown, setIsShown] = useState<boolean>(false);

  return (
    <section className={classes.moveList}>
      <button onClick={() => setIsShown(!isShown)}>Moves</button>
      <hr />
      {isShown &&
        moves.map(move => (
          <div key={move.move.name} className={classes.name}>
            {move.move.name.replace("-", " ")}
          </div>
        ))}
    </section>
  );
}

const styles = {
  moveList: {
    width: "100%",
    margin: {
      left: 20
    }
  },
  name: {
    textTransform: "capitalize"
  }
};

export const Moves = injectSheet(styles)(MovesComponent);
