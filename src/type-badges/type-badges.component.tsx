import React from "react";
import injectSheet from "react-jss";
import { Type, TypeContainer } from "../make-request/poke-api.types";

type TypeBadgesProps = {
  classes: any;
  types: TypeContainer[];
};

export function TypeBadgesComponent({
  classes,
  types
}: TypeBadgesProps): React.ReactElement<TypeBadgesProps> {
  const orderedTypes = types.sort((a, b) => a.slot - b.slot);
  const getBackgroundStyles = (type: Type) => {
    let backgroundColor;
    let fontColor = "black";
    switch (type) {
      case Type.Fighting:
        backgroundColor = "red";
        break;
      case Type.Flying:
        backgroundColor = "lightskyblue";
        break;
      case Type.Poison:
        backgroundColor = "purple";
        break;
      case Type.Ground:
        backgroundColor = "sandybrown";
        break;
      case Type.Rock:
        backgroundColor = "tan";
        break;
      case Type.Bug:
        backgroundColor = "springgreen";
        break;
      case Type.Ghost:
        backgroundColor = "slateblue";
        break;
      case Type.Steel:
        backgroundColor = "silver";
        break;
      case Type.Fire:
        backgroundColor = "orange";
        break;
      case Type.Water:
        backgroundColor = "blue";
        break;
      case Type.Grass:
        backgroundColor = "green";
        break;
      case Type.Electric:
        backgroundColor = "yellow";
        break;
      case Type.Psychic:
        backgroundColor = "magenta";
        break;
      case Type.Ice:
        backgroundColor = "aliceblue";
        break;
      case Type.Dragon:
        backgroundColor = "blueviolet";
        break;
      case Type.Dark:
        backgroundColor = "black";
        fontColor = "white";
        break;
      case Type.Fairy:
        backgroundColor = "lightpink";
        break;
      case Type.Normal:
      default:
        backgroundColor = "wheat";
        break;
    }
    return {
      backgroundColor,
      color: fontColor
    };
  };

  return (
    <section className={classes.container}>
      {orderedTypes.map(type => (
        <div
          key={type.slot}
          className={classes.badge}
          style={getBackgroundStyles(type.type.name)}
          data-testid="typeName"
        >
          {type.type.name}
        </div>
      ))}
    </section>
  );
}

const styles = {
  container: {
    display: "flex"
  },
  badge: {
    backgroundColor: "wheat",
    fontSize: 15,
    border: "1px grey solid",
    borderRadius: 20,
    padding: "2px 5px",
    margin: {
      right: 2
    }
  }
};

export const TypeBadges = injectSheet(styles)(TypeBadgesComponent);
