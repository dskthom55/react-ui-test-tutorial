import React from "react";
import injectSheet from "react-jss";
import { useLoadPokemon } from "../make-request/make-request";
import { Loader } from "../loader";
import { StatBlock } from "../stat-block";
import { TypeBadges } from "../type-badges";
import { Moves } from "../moves";

type PokemonProps = {
  pokemonId: number;
};

type Props = PokemonProps & {
  classes: any;
};

function PokemonComponent({
  classes,
  pokemonId
}: Props): React.ReactElement<Props> {
  const { pokemon, error } = useLoadPokemon(pokemonId);

  let content = <Loader />;
  if (pokemon) {
    content = (
      <>
        <header className={classes.header}>
          <img
            alt={`${pokemon.name} sprite`}
            src={pokemon.sprites.front_default}
            className={classes.sprite}
          />
          <span>
            <h1 className={classes.name}>
              <div>
                #{pokemonId} {pokemon.name}
              </div>
              <TypeBadges types={pokemon.types} />
            </h1>
          </span>
        </header>
        <hr />
        <section className={classes.specs}>
          <div>
            <h1>Stats</h1>
            <StatBlock stats={pokemon.stats} />
          </div>
          <Moves moves={pokemon.moves} />
        </section>
      </>
    );
  }
  if (error) {
    content = (
      <h1>
        Error occurred while loading Pokemon!
        <br />
        Verify that you have a valid Pokemon ID!
      </h1>
    );
  }

  return <article className={classes.article}>{content}</article>;
}

const styles = {
  article: {
    margin: 10
  },
  header: {
    display: "flex"
  },
  sprite: {
    border: "1px grey solid",
    borderRadius: 20,
    marginRight: 20
  },
  name: {
    textTransform: "capitalize",
    fontSize: 30
  },
  specs: {
    display: "flex"
  }
};

export const Pokemon = injectSheet(styles)(PokemonComponent);
