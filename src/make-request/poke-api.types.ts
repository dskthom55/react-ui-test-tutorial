export type NameUrlPair = {
  name: string;
  url: string;
};

export type Ability = {
  ability: NameUrlPair;
  is_hidden: boolean;
};

export type Move = {
  move: NameUrlPair;
};

export type Sprites = {
  back_default: string;
  back_female: string;
  back_shiny: string;
  back_shiny_female: string;
  front_default: string;
  front_female: string;
  front_shiny: string;
  front_shiny_female: string;
};

export enum Stat {
  HP = "hp",
  Attack = "attack",
  Defense = "defense",
  SpecialAttack = "special-attack",
  SpecialDefense = "special-defense",
  Speed = "speed"
}

export type StatContainer = {
  base_stat: number;
  effort: number;
  stat: {
    name: Stat;
    url: string;
  };
};

export enum Type {
  Normal = "normal",
  Fighting = "fighting",
  Flying = "flying",
  Poison = "poison",
  Ground = "ground",
  Rock = "rock",
  Bug = "bug",
  Ghost = "ghost",
  Steel = "steel",
  Fire = "fire",
  Water = "water",
  Grass = "grass",
  Electric = "electric",
  Psychic = "psychic",
  Ice = "ice",
  Dragon = "dragon",
  Dark = "dark",
  Fairy = "fairy"
}

export type TypeContainer = {
  slot: number;
  type: {
    name: Type;
    url: string;
  };
};

export type PokemonType = {
  abilities: Ability[];
  base_experience: number;
  height: number; // decimeters???
  moves: Move[];
  name: string;
  sprites: Sprites;
  stats: StatContainer[];
  types: TypeContainer[];
  weight: number;
};
