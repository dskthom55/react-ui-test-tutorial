import React from "react";
import {
  BrowserRouter,
  NavLink,
  Route,
  RouteComponentProps,
  Switch
} from "react-router-dom";
import { Pokemon } from "./pokemon";

const MAX_POKEMON_ID = 807;
function getRandomPokemonId(): number {
  return Math.floor(Math.random() * MAX_POKEMON_ID);
}

export function App(): React.ReactElement<any> {
  return (
    <section>
      <BrowserRouter>
        <Switch>
          <Route
            path="/pokemon/:id"
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <Pokemon pokemonId={+match.params.id} />
            )}
          />
          <Route path="">
            <NavLink to={() => `/pokemon/${getRandomPokemonId()}`}>
              See random Pokemon
            </NavLink>
          </Route>
        </Switch>
      </BrowserRouter>
    </section>
  );
}
